define("AccountPageV2", [], function() {
	return {
		entitySchemaName: "Account",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"GbcAccAccountType": {
				"78f6202a-e0bc-4618-a9f3-fba596092691": {
					"uId": "78f6202a-e0bc-4618-a9f3-fba596092691",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 3,
								"value": "CurrentUser",
								"dataValueType": 10
							},
							"rightExpression": {
								"type": 3,
								"value": "CurrentUser",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"GbcAccountINN": {
				"bb560e49-f6f2-48ba-b3af-a2143da2948c": {
					"uId": "bb560e49-f6f2-48ba-b3af-a2143da2948c",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "GbcAccAccountType"
							},
							"rightExpression": {
								"type": 0,
								"value": "6d4cdf42-d850-4c79-ba2f-2853e41686ca",
								"dataValueType": 10
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "AccountPhotoContainer",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountName",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountType",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountOwner",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountWeb",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountPhone",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5
					}
				}
			},
			{
				"operation": "merge",
				"name": "NewAccountCategory",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountIndustry",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 7
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountPageGeneralTabContainer",
				"values": {
					"order": 0
				}
			},
			{
				"operation": "merge",
				"name": "AlternativeName",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Code",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "EmployeesNumber",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Ownership",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "AnnualRevenue",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "insert",
				"name": "GbcTabAccountRequisites",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.GbcTabAccountRequisitesTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "GbcTabAccountRequisitesGroup",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.GbcTabAccountRequisitesGroupGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "GbcTabAccountRequisites",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "GbcTabAccountRequisitesGridLayout1b28f8e2",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "GbcTabAccountRequisitesGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUP09f42fa0-54b2-4259-ba39-bea46704f1ba",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "GbcTabAccountRequisitesGridLayout1b28f8e2"
					},
					"bindTo": "GbcAccAccountType",
					"tip": {
						"content": {
							"bindTo": "Resources.Strings.LOOKUP09f42fa054b24259ba39bea46704f1baTip"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "GbcTabAccountRequisitesGridLayout1b28f8e2",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRINGeeb70cae-4dbc-4933-97ba-997120ea76f2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "GbcTabAccountRequisitesGridLayout1b28f8e2"
					},
					"bindTo": "GbcAccountINN",
					"enabled": true
				},
				"parentName": "GbcTabAccountRequisitesGridLayout1b28f8e2",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "STRING947be151-ecb6-406b-851f-21bf1d0399af",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "GbcTabAccountRequisitesGridLayout1b28f8e2"
					},
					"bindTo": "GbcAccountOGRN",
					"enabled": true
				},
				"parentName": "GbcTabAccountRequisitesGridLayout1b28f8e2",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "STRING04583f1e-8d90-4d3b-9b7f-55f3def0c6d5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "GbcTabAccountRequisitesGridLayout1b28f8e2"
					},
					"bindTo": "GbcAccountKPP",
					"enabled": true
				},
				"parentName": "GbcTabAccountRequisitesGridLayout1b28f8e2",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "merge",
				"name": "ContactsAndStructureTabContainer",
				"values": {
					"order": 2
				}
			},
			{
				"operation": "merge",
				"name": "RelationshipTabContainer",
				"values": {
					"order": 3
				}
			},
			{
				"operation": "merge",
				"name": "HistoryTabContainer",
				"values": {
					"order": 4
				}
			},
			{
				"operation": "merge",
				"name": "NotesTabContainer",
				"values": {
					"order": 5
				}
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 6
				}
			},
			{
				"operation": "merge",
				"name": "TimelineTab",
				"values": {
					"order": 5
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
