define("GbcDelivery1Page", [], function() {
	return {
		entitySchemaName: "GbcDelivery",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"Files": {
				"schemaName": "FileDetailV2",
				"entitySchemaName": "GbcDeliveryFile",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "GbcDelivery"
				}
			},
			"GbcSchema18427199Detaileeaae812": {
				"schemaName": "GbcSchema18427199Detail",
				"entitySchemaName": "OrderProduct",
				"filter": {
					"detailColumn": "Order",
					"masterColumn": "GbcOrder"
				}
			},
			"GbcSchemaDeliveryActivityHistoryDetail": {
				"schemaName": "GbcSchema2437202dDetail",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "GbcGbcDelivery",
					"masterColumn": "Id"
				}
			},
			"GbcSchemaDeliveryDetailStatusHistory": {
				"schemaName": "GbcSchema1525ff46Detail",
				"entitySchemaName": "GbcDeliveryStatusHistory",
				"filter": {
					"detailColumn": "GbcGbcDelivery",
					"masterColumn": "Id"
				}
			},
			"GbcSchemaDetailProductInOrderHandmadeVersion": {
				"schemaName": "GbcSchemaa025f3bdDetail",
				"entitySchemaName": "OrderProduct",
				"filter": {
					"detailColumn": "GbcDelivery",
					"masterColumn": "Id"
				}
			},
			"VisaDetailV23039a15e": {
				"schemaName": "VisaDetailV2",
				"entitySchemaName": "GbcDeliveryVisa",
				"filter": {
					"masterColumn": "Id",
					"detailColumn": "GbcDelivery"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"GbcTotalSum": {
				"b03c27e1-f755-47f7-9f9f-66be26478921": {
					"uId": "b03c27e1-f755-47f7-9f9f-66be26478921",
					"enabled": true,
					"removed": false,
					"ruleType": 3,
					"populatingAttributeSource": {
						"expression": {
							"type": 6,
							"formula": {
								"type": 0,
								"dataType": 4,
								"operatorType": 1,
								"leftExpression": {
									"type": 1,
									"dataType": 4,
									"operandType": 1,
									"columnPath": "GbcDeliveryPrice",
									"columnOperandType": 0
								},
								"rightExpression": {
									"type": 1,
									"dataType": 4,
									"operandType": 1,
									"columnPath": "GbcOrderSum",
									"columnOperandType": 0
								},
								"arithmeticOperatorType": 0
							}
						}
					},
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "GbcDeliveryPrice"
							}
						},
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "GbcOrderSum"
							}
						}
					]
				}
			},
			"GbcAccount": {
				"f6571c00-16eb-46da-bfb8-c74b3149632a": {
					"uId": "f6571c00-16eb-46da-bfb8-c74b3149632a",
					"enabled": true,
					"removed": false,
					"ruleType": 3,
					"populatingAttributeSource": {
						"expression": {
							"type": 1,
							"attribute": "GbcOrder",
							"attributePath": "Account"
						}
					},
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "GbcOrder"
							}
						}
					]
				}
			},
			"GbcContact": {
				"12f0f009-d023-4129-86a4-1e7f08474694": {
					"uId": "12f0f009-d023-4129-86a4-1e7f08474694",
					"enabled": true,
					"removed": false,
					"ruleType": 3,
					"populatingAttributeSource": {
						"expression": {
							"type": 1,
							"attribute": "GbcOrder",
							"attributePath": "Contact"
						}
					},
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "GbcOrder"
							}
						}
					]
				}
			},
			"GbcDeliveryTo": {
				"a817d670-df69-4906-9424-7229e27acd16": {
					"uId": "a817d670-df69-4906-9424-7229e27acd16",
					"enabled": true,
					"removed": false,
					"ruleType": 3,
					"populatingAttributeSource": {
						"expression": {
							"type": 1,
							"attribute": "GbcOrder",
							"attributePath": "Contact.Name"
						}
					},
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "GbcOrder"
							}
						}
					]
				}
			},
			"GbcDeliveryPhone": {
				"d2288c18-4531-4ba6-b543-e3743d2d8e67": {
					"uId": "d2288c18-4531-4ba6-b543-e3743d2d8e67",
					"enabled": true,
					"removed": false,
					"ruleType": 3,
					"populatingAttributeSource": {
						"expression": {
							"type": 1,
							"attribute": "GbcOrder",
							"attributePath": "Contact.MobilePhone"
						}
					},
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 2,
							"leftExpression": {
								"type": 1,
								"attribute": "GbcOrder"
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "GbcName1c65419a-dc0f-4f79-8aa8-921e43095366",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "ProfileContainer"
					},
					"bindTo": "GbcName"
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUPc31b678c-9ad9-48a6-b6c2-819d84c384eb",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "GbcOrder",
					"tip": {
						"content": {
							"bindTo": "Resources.Strings.LOOKUPc31b678c9ad948a6b6c2819d84c384ebTip"
						}
					},
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING446d4ca4-9f69-4866-a5e1-8ede98908c86",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "GbcAddress",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "GbcAccountb99dd8e9-2484-4fab-a646-676eaa47a347",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "GbcAccount"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "GbcContact5bd7f4ec-9a00-4d89-833b-8705aaa1af8b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "GbcContact"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUPcc4bbd9d-6c2c-4ce1-ba8d-664464fbe70c",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "GbcDeliveryStatus",
					"enabled": false,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "LOOKUP8b8c11a4-9a6e-427c-86c2-df4286cd2b74",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "GbcDeliveryPriority",
					"enabled": false,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "CreatedBy5c8431e0-1f6a-4ecf-a39f-e2df4e197c32",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "CreatedBy"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "GbcDeliveryTabBaseInfo",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.DeliveryTabBaseInfoTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "GbcDeliveryTabBaseInfoGroupMain",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.GbcDeliveryTabBaseInfoGroupMainGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "GbcDeliveryTabBaseInfo",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "GbcDeliveryTabBaseInfoGridLayout16a08faf",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "GbcDeliveryTabBaseInfoGroupMain",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "INTEGERd76bb512-c9af-4d76-a678-786a5a267e1a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "GbcDeliveryTabBaseInfoGridLayout16a08faf"
					},
					"bindTo": "GbcOrderSum",
					"enabled": true
				},
				"parentName": "GbcDeliveryTabBaseInfoGridLayout16a08faf",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "INTEGER2c675ccf-ff58-433b-b2af-dfec9427906e",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "GbcDeliveryTabBaseInfoGridLayout16a08faf"
					},
					"bindTo": "GbcDeliveryPrice",
					"enabled": true
				},
				"parentName": "GbcDeliveryTabBaseInfoGridLayout16a08faf",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "INTEGER54432904-be49-4391-a942-20c2f6e16ab2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "GbcDeliveryTabBaseInfoGridLayout16a08faf"
					},
					"bindTo": "GbcTotalSum",
					"enabled": true
				},
				"parentName": "GbcDeliveryTabBaseInfoGridLayout16a08faf",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "GbcDeliveryTabBaseInfoGroupDeliveryInfo",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.GbcDeliveryTabBaseInfoGroupDeliveryInfoGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "GbcDeliveryTabBaseInfo",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "GbcDeliveryTabBaseInfoGridLayout8d5be6cd",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "GbcDeliveryTabBaseInfoGroupDeliveryInfo",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING6565bfa6-7ccc-4642-9cd1-620ed47bd994",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "GbcDeliveryTabBaseInfoGridLayout8d5be6cd"
					},
					"bindTo": "GbcDeliveryTo",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "GbcDeliveryTabBaseInfoGridLayout8d5be6cd",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRINGa722fbd3-3aa2-493a-99a3-241c6ae8f314",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "GbcDeliveryTabBaseInfoGridLayout8d5be6cd"
					},
					"bindTo": "GbcDeliveryPhone",
					"enabled": true
				},
				"parentName": "GbcDeliveryTabBaseInfoGridLayout8d5be6cd",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "LOOKUPc662908b-7fe4-47b8-806d-9b4418b7522d",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "GbcDeliveryTabBaseInfoGridLayout8d5be6cd"
					},
					"bindTo": "GbcDeliveryType",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "GbcDeliveryTabBaseInfoGridLayout8d5be6cd",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "STRING9b7dab4e-b962-40c8-83fa-857db94ebed5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "GbcDeliveryTabBaseInfoGridLayout8d5be6cd"
					},
					"bindTo": "GbcDeliveryComment",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "GbcDeliveryTabBaseInfoGridLayout8d5be6cd",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "GbcSchema18427199Detaileeaae812",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "GbcDeliveryTabBaseInfo",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "GbcSchemaDeliveryDetailStatusHistory",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "GbcDeliveryTabBaseInfo",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "GbcSchemaDetailProductInOrderHandmadeVersion",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "GbcDeliveryTabBaseInfo",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "GbcTabDeliveryHistory",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.GbcTabDeliveryHistoryTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "GbcSchemaDeliveryActivityHistoryDetail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "GbcTabDeliveryHistory",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 2
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Files",
				"values": {
					"itemType": 2
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesControlGroup",
				"values": {
					"itemType": 15,
					"caption": {
						"bindTo": "Resources.Strings.NotesGroupCaption"
					},
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "Notes",
				"values": {
					"bindTo": "GbcNotes",
					"dataValueType": 1,
					"contentType": 4,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24
					},
					"labelConfig": {
						"visible": false
					},
					"controlConfig": {
						"imageLoaded": {
							"bindTo": "insertImagesToNotes"
						},
						"images": {
							"bindTo": "NotesImagesCollection"
						}
					}
				},
				"parentName": "NotesControlGroup",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 3
				}
			},
			{
				"operation": "insert",
				"name": "Tab4fb05e06TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.TabVisaCaption"
					},
					"items": [],
					"order": 4
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "VisaDetailV23039a15e",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "Tab4fb05e06TabLabel",
				"propertyName": "items",
				"index": 0
			}
		]/**SCHEMA_DIFF*/
	};
});
