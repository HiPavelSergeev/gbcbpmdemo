define("LeadPageV2", [], function() {
	return {
		entitySchemaName: "Lead",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"GbcLeadAccountType": {
				"8612061d-6fe3-46df-8c8a-f7277ff6b27d": {
					"uId": "8612061d-6fe3-46df-8c8a-f7277ff6b27d",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 3,
								"value": "CurrentUserContact",
								"dataValueType": 10
							},
							"rightExpression": {
								"type": 3,
								"value": "CurrentUserContact",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"GbcLeadINN": {
				"d30a1118-4189-4d07-98e2-484d171a690e": {
					"uId": "d30a1118-4189-4d07-98e2-484d171a690e",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 2,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 1,
								"attribute": "GbcLeadAccountType"
							},
							"rightExpression": {
								"type": 0,
								"value": "6d4cdf42-d850-4c79-ba2f-2853e41686ca",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"LeadSource": {
				"b239df3d-51fc-49b4-b4d8-e41bc8fe7a1c": {
					"uId": "b239df3d-51fc-49b4-b4d8-e41bc8fe7a1c",
					"enabled": true,
					"removed": false,
					"ruleType": 1,
					"baseAttributePatch": "LeadMedium",
					"comparisonType": 3,
					"autoClean": true,
					"autocomplete": true,
					"type": 1,
					"attribute": "LeadMedium"
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "GbcLeadRequisites",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.GbcLeadRequisitesGroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "GeneralInfoTabGridLayoutfcb9b90b",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "GbcLeadRequisites",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUPd73d3910-6cce-440c-88ec-e293bda43ec7",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "GeneralInfoTabGridLayoutfcb9b90b"
					},
					"bindTo": "GbcLeadAccountType",
					"tip": {
						"content": {
							"bindTo": "Resources.Strings.LOOKUPd73d39106cce440c88ece293bda43ec7Tip"
						}
					},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "GeneralInfoTabGridLayoutfcb9b90b",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRINGd693335f-7469-436c-8561-ad4ed43b96f4",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "GeneralInfoTabGridLayoutfcb9b90b"
					},
					"bindTo": "GbcLeadINN",
					"enabled": true
				},
				"parentName": "GeneralInfoTabGridLayoutfcb9b90b",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "STRING4f1cdd67-7d60-42a2-a905-65b5995b0916",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "GeneralInfoTabGridLayoutfcb9b90b"
					},
					"bindTo": "GbcLeadOGRN",
					"enabled": true
				},
				"parentName": "GeneralInfoTabGridLayoutfcb9b90b",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "STRINGf5567c74-a8bf-43aa-9122-9d8952f0a524",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "GeneralInfoTabGridLayoutfcb9b90b"
					},
					"bindTo": "GbcLeadKPP",
					"enabled": true
				},
				"parentName": "GeneralInfoTabGridLayoutfcb9b90b",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "merge",
				"name": "DealSpecificsTab",
				"values": {
					"order": 5
				}
			},
			{
				"operation": "merge",
				"name": "HistoryTab",
				"values": {
					"order": 6
				}
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 8
				}
			},
			{
				"operation": "merge",
				"name": "NotesTab",
				"values": {
					"order": 7
				}
			},
			{
				"operation": "merge",
				"name": "TimelineTab",
				"values": {
					"order": 6
				}
			},
			{
				"operation": "move",
				"name": "SalesOwnerV2",
				"parentName": "LeadPageSQLTabGridLayout",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "move",
				"name": "OpportunityDepartmentV2",
				"parentName": "LeadPageSQLTabGridLayout",
				"propertyName": "items",
				"index": 6
			}
		]/**SCHEMA_DIFF*/
	};
});
